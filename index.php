<?php
$title = "eCommerce Gaming";
require "./src/header.php";
$category_id = -1;
if (isset($_GET['category_id'])):
    $category_id = $_GET['category_id'];
endif;
?>
    <nav class="nav nav-pills">
        <li class="nav-item">
            <a class="nav-link<?php if ($category_id == -1): ?> active<?php endif; ?>" href="./index.php">Tous nos
                produits</a>
        </li>
        <?php if (isset($categories)): ?>
            <?php foreach ($categories as $category): ?>
                <li class="nav-item">
                    <a class="nav-link<?php if ($category_id == $category['id']): ?> active<?php endif; ?>"
                       href="./index.php?category_id=<?php echo $category['id']; ?>"><?php echo $category['name']; ?></a>
                </li>
            <?php endforeach; ?>
        <?php endif; ?>
    </nav>
<?php if (isset($products)):
    if ($category_id != -1):
        $products = filter_products_by_category_id($products, $category_id);
    endif;
    ?>
    <?php if (sizeof($products) > 0):?>
    <div class="row row-cols-1 row-cols-md-3 mt-4">
        <?php foreach ($products as $product): ?>
            <div class="col">
                <div class="card mb-4">
                    <img src="https://picsum.photos/id/531/300/200?grayscale" class="card-img-top">
                    <div class="card-body">
                        <h2 class="h5"><?php echo $product['name']; ?></h2>
                        <span class="badge badge-primary"><?php echo get_category_name_by_id($categories, $product['category_id']); ?></span>
                    </div>
                    <div class="card-footer">
                        <?php echo $product['price']; ?>€
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php else: ?>
    <div class="alert alert-success mt-4">Aucun produit</div>
<?php endif; ?>
<?php else: ?>
    <div class="alert alert-success mt-4">Aucun produit</div>
<?php endif; ?>
<?php
require "./src/footer.php";
?>