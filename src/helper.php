<?php
/**
 * Afficher le nom de la catégorie à partir de son id
 * @param $categories
 * @param $id
 * @return string - nom de la catégory, chaîne de caractère vide si la catégorie n'est pas trouvé
 */
function get_category_name_by_id($categories, $id) {
    foreach ($categories as $category):
        if($category['id'] == $id):
            return $category['name'];
        endif;
    endforeach;
    return "";
}

/**
 * Filtre la liste des produits en fonction d'un id de catégorie
 * @param $products
 * @param $category_id
 * @return array
 */
function filter_products_by_category_id($products, $category_id) {
    $result = [];
    foreach ($products as $product):
        if($product['category_id'] == $category_id):
            $result[] = $product;
        endif;
    endforeach;
    return $result;
}