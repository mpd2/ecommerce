<?php
$categories = [];
for($i = 1; $i <= 4; $i++):
    $categories[] = [
            "id" => $i,
            "name" => "Catégorie $i",
    ];
endfor;
$products = [];
$generated_product_id = 1;
for($i = 1; $i <= rand(40, 120); $i++):
    $products[] = [
        "id" => $generated_product_id,
        "name" => "Produit ". $generated_product_id,
        "price" => rand(49, 799),
        "category_id" => rand(1,sizeof($categories)-1)
    ];
    $generated_product_id ++;
endfor;

require "helper.php";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo isset($title) ? $title : "eCommerce Gaming"; ?></title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
          integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="./resources/app.css"/>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="./index.php"><?php echo "eCommerce Gaming"; ?></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="./account.php">Mon compte</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="./cart.php">Mon panier</a>
            </li>
        </ul>
    </div>
</nav>
<main role="main" class="container">